 <details>
    <summary> Best Repo So far </summary>

- https://github.com/jmw5598/node-typescript-express-starter

</details>

# 1st steps
 ```bash
    npm init
    touch README.md
    npm i --save express cors 
    npm install nodemon --save-dev
    @types/node @types/express ts-node typescript --save-dev

 ```

 ### para nodemon en `package.json`:
 __ __`"dev": "nodemon ./server.js localhost 3080"`____ configs for **`npm run dev`** command

 ---------------------
 
 - configurar el `tsconfig.json` con `tsc --init`
 - alternativa ts-node y configurar el watch en `nodemon.json`. **aki estara el entrypoinnt de nuestra app** `"exec": "ts-node ./src/index.ts"`
 - production build : https://khalilstemmler.com/blogs/typescript/node-starter-project/#Creating-production-builds
- DotEnv() `npm install dotenv`, (remember .gitignore) 
```typescript
dotenv.config();
...
 process.env.ENVIRONMENT_VARIABLE //ready to use,
```
- server.ts
```typescript
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
require('dotenv').config();

const app = express()
  .use(cors())
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: true }))
  ;

const router = express.Router()
/*Ruta default*/
app.all('/', (req, res) => res.send('Express + TypeScript Server ROOT'));

/*Starting server*/
const PORT  = process.env.PORT || 8000;
app.listen(PORT, () => {
  return console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
});

```
- para iniciar en modo normal, con SSL(no incognito), hay que instalar `https + fs` modules
- status Codes:`npm i http-status-codes --save`
200 - OK
201 - Created  # Response to successful POST or PUT
302 - Found # Temporary redirect such as to /login
303 - See Other # Redirect back to page after successful login
304 - Not Modified
400 - Bad Request
401 - Unauthorized  # Not logged in
403 - Forbidden  # Accessing another user's resource
404 - Not Found
500 - Internal Server Error
# typeORM and preparing for crud
```bash
npm i sqlite3 typeorm reflect-metadata --save
npm install @types/node --save-dev
```
<details>
    <summary> en `tsconfig.json` </summary>

```javascript
"emitDecoratorMetadata": true,
"experimentalDecorators": true,
```
</details>
- database in /data/bd_name.db

<details>
    <summary> en `ormconfig.json` </summary>
this file offers support for multiple conection configs

```javascript
{
    "type": "mysql",
    "database": "data/testdatadb.db",
    "synchronize": true,
    "logging": false,
    "entities": [
       "src/entity/**/*.ts"
    ]
}
```
</details>
- Conection; promise based; || can be done with async Await also.
<details>
    <summary> en `testRoute.ts` </summary>
this file offers support for multiple conection configs

```typescript
import {createConnection} from "typeorm";
import {Photo} from "./entity/Photo";

createConnection().then(async connection => {

    let photo = new Photo();
    photo.name = "Me and Bears";
    photo.description = "I am near polar bears";
    photo.filename = "photo-with-bears.jpg";
    photo.views = 1;
    photo.isPublished = true;

    let photoRepository = connection.getRepository(Photo);

    await photoRepository.save(photo);
    console.log("Photo has been saved");

    let savedPhotos = await photoRepository.find();
    console.log("All photos from the db: ", savedPhotos);

}).catch(error => console.log(error));
```
</details>

- Integrando routing controllers `npm i routing-controllers multer @types/multer typeorm-routing-controllers-extensions class-transformer class-validator--save`
//chekear tuto y codigo porque es una movida

- chek rama _SOLIDpara estructura del codigo segun best repof-So-Far.

