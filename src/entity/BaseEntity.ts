import { Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('BaseEntity')
export abstract class BaseEntity {
    @PrimaryGeneratedColumn()
    public id: number;
}