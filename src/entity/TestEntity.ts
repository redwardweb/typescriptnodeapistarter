import {Entity, Column} from "typeorm";
import { BaseEntity } from "./BaseEntity";

@Entity('TestEntity')
export default class TestEntity extends BaseEntity {
    
    @Column({ unique: true })
    name: string;

    @Column()
    description: string;

}