import  { Router } from 'express';
import { StatusCodes } from 'http-status-codes';
import testRoute from './testRoute';

const router = Router();
/**creo la conexioon como middlewarte y 
 * luego lo uso en rutas con reporsitory */
/*subrutas de la /API1/ REST*/
router.get("/", (req, res) => {
    res.status(StatusCodes.OK).json(
        "hellow From Api with ROUNTING"
    );
});
router.use('/test',testRoute);
//router.use(); subrutas o handler directo

/*ruta unmatched or all=== not found*/
router.use(function(req, res){
    res.sendStatus(StatusCodes.NOT_FOUND);
 });
export default router;