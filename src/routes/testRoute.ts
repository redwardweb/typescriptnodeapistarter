import  { Router } from 'express';
import { StatusCodes } from 'http-status-codes';
import { createConnection } from "typeorm";
import  TestEntity  from "../entity/TestEntity";

const testRoute = Router();
//creamos conexion luego registramos rutas
/**la conexion a db se definea nivel de server y dentro se inicializa la app
 * , then router y modulos a nivel de app etc
 */

/**Ruta default /api/test*/
testRoute.get('/', (req, res) =>{
    // Statements iside here 
    createConnection().then(async connection => {
        let photo = new TestEntity();
        photo.name = "Me and Bears5";
        photo.description = "I am near polar bears";
    
        let testRepository = connection.getRepository(TestEntity);
    
        await testRepository.save(photo);
        console.log("Photo has been saved");
    
        let savedPhotos = await testRepository.find();
        console.log("All photos from the db: ", savedPhotos);
    })
    .catch(error => console.log(error));
    
        res.status(StatusCodes.NOT_FOUND).json({
            data:'Welcome to /API/test'
        });
        console.log("Entered Api/test/");
    }
 );

/*ruta unmatched or all=== not found*/
testRoute.use(function(req, res){
    res.sendStatus(StatusCodes.NOT_FOUND);
 });
export default testRoute;