import { response } from "express";
import { Get, JsonController, Post, Req } from "routing-controllers";
import { getRepository, Repository } from "typeorm";
import TestEntity from "../entity/TestEntity";


@JsonController("/test")
export default class TestController{
    private testRepo: Repository<TestEntity>;
    
    constructor() {
        this.testRepo = getRepository(TestEntity);
    }

    @Get("/")
    getAll() {
       return this.testRepo.find();
    }
    @Get("/1")
    test(@Req() request: any,) {
        console.log(request);
       return this.testRepo.find();
    }

}