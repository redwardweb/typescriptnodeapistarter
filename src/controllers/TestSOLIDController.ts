import { Body, Delete, Get, JsonController, Param, Post, Put } from "routing-controllers";
import { getRepository, Repository } from "typeorm";
import TestEntity from "../entity/TestEntity";


@JsonController("/testSolid")
export default class TestController{
    private testRepo: Repository<TestEntity>;
    
    constructor() {
        this.testRepo = getRepository(TestEntity);
    }

    @Get("/")
    getAll() {
       return this.testRepo.find();
    }

    @Get("/entidadTest/:id")
    getOne(@Param("id") id: number) {
       return this.testRepo.findOne(id);
    }

    @Post("/entidadTest")
    post(@Body() test: TestEntity) {
       return this.testRepo.insert(test);
    }

    @Put("/entidadTest/:id")
    put(@Param("id") id: number, @Body() test: TestEntity) {
       return this.testRepo.save(test);
    }

    @Delete("/entidadTest/:id")
    remove(@Body() test: TestEntity) {
       return this.testRepo.remove(test);
    }

}