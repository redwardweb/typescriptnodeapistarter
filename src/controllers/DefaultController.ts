import { Get, JsonController } from "routing-controllers";

@JsonController()
export default class DefaultController{
    @Get("/")
    getAll() {
       return "Hello from /api1 with Defaultcontroller";
    }

}