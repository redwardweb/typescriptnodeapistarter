import express,{ Request, Response } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
require('dotenv').config();
import {StatusCodes}  from "http-status-codes";
import router from './routes/main';
import { useExpressServer } from 'routing-controllers';
import DefaultController from './controllers/DefaultController';
import TestController from './controllers/TestController';
import TestSOLIDController from './controllers/TestSOLIDController';

import { createConnection } from 'typeorm';


createConnection().then(async () => {
  console.log("DB CONECTED");

  const app = express()
  .use(cors())
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: true }))
  ;
  /**Registering controllers */
  useExpressServer(app,{
    cors:true,
    routePrefix: "/api1",
    controllers: [
      DefaultController,
      TestController, 
      TestSOLIDController
    ] 
  })
/*Ruta default*/
app.get('/', (req: Request, res: Response) =>
    //res.send('Express + TypeScript Server ROOT')
    res.status(StatusCodes.OK).json({
        helo: "hi!", 
        data:'Express + TypeScript Server ROOT'
    })
 );

/**Rutas*/
app.use('/api',router);
 
/*ruta unmatched or all=== not found, fuynciona como middleware*/
app.use(function(req: Request, res:Response){
    res.sendStatus(404);
 });
/*Starting server*/
const PORT  = process.env.PORT || 8000;
app.listen(PORT, () => {
  return console.log(`⚡️[server]: Server is running at http://localhost:${PORT}`);
});
}).catch(error => console.log(error));
