import {  } from "module";
import TestEntity from "src/entity/TestEntity";
import { getRepository } from "typeorm";

export default class TestRepository{
    constructor(){
        return getRepository(TestEntity);
    }
}